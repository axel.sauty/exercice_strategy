package client;

import strategy.BubblesortStrategy;
import strategy.MergesortStrategy;
import strategy.QuicksortStrategy;
import strategy.SortStrategy;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // preparing strategies
        SortStrategy quicksort = new QuicksortStrategy();
        SortStrategy mergesort = new MergesortStrategy();
        SortStrategy bubblesort = new BubblesortStrategy();
        // preparing context
        Ranking ranking = new Ranking(quicksort);
        // preparing data
        ArrayList<Integer> ranks = new ArrayList();
        ranking.rank(ranks);

        // switching strategy
        ranking.setStrategy(mergesort);
        ranking.rank(ranks);

        // switching strategy
        ranking.setStrategy(bubblesort);
        ranking.rank(ranks);

        // TODO: call new strategy here

        // TODO: create new context here
    }
}