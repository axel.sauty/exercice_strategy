package client;

import strategy.SortStrategy;

import java.util.ArrayList;

public class Ranking {

    private SortStrategy strategy;

    public Ranking(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void setStrategy(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void rank(ArrayList<Integer> ranks) {
        strategy.execute(ranks);
    }
}
