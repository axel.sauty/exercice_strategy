package strategy;

import java.util.ArrayList;

public interface SortStrategy {

    void execute(ArrayList<Integer> list);

}
