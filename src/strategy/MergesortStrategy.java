package strategy;

import java.util.ArrayList;

public class MergesortStrategy implements SortStrategy {
    @Override
    public void execute(ArrayList<Integer> list) {
        System.out.println("Executing sort with strategy: merge sort\nProcessing...\nDone");
    }
}
