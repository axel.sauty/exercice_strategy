package strategy;

import java.util.ArrayList;

public class QuicksortStrategy implements SortStrategy {
    @Override
    public void execute(ArrayList<Integer> list) {
        System.out.println("Executing sort with strategy: quicksort\nProcessing...\nDone");
    }
}
