package strategy;

import java.util.ArrayList;

public class BubblesortStrategy implements SortStrategy {
    @Override
    public void execute(ArrayList<Integer> list) {
        System.out.println("Executing sort with strategy: bubble sort\nProcessing...\nDone");
    }
}
